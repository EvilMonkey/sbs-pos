import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, PRIMARY_OUTLET, Router } from '@angular/router';
import { RouterData } from './layout-interface';
import { CustomDialogService } from '../shared/dialog/dialog.service';
import { AuthService } from '../modules/auth/auth.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {

  protected $routerSub: Subscription
  public routeterDatas: RouterData[] = []

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private customDialogService: CustomDialogService,
    private authService: AuthService
  ) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        const root: ActivatedRoute = this.route.root
        this.routeterDatas = this.getRouterData(root)
      })
  }

  private getRouterData(
    route: ActivatedRoute,
    url: string = '',
    routerDatas: RouterData[] = []
  ): RouterData[] {
    const ROUTE_DATA_BREADCRUMB = 'title'
    // get the child routes
    const children: ActivatedRoute[] = route.children

    if (children.length === 0) {
      return routerDatas
    }

    // iterate over each children
    for (const child of children) {
      // verify primary route
      if (child.outlet !== PRIMARY_OUTLET || child.snapshot.url.length === 0) {
        continue
      }

      // verify the custom data property "breadcrumb" is specified on the route

      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.getRouterData(child, url, routerDatas)
      }

      // get the route's URL segment
      const routeURL: string = child.snapshot.url
        .map((segment) => segment.path)
        .join('/')

      // append route URL to URL
      url += `/${routeURL}`

      // add breadcrumb
      const routerData: RouterData = {
        title: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        url,
      }
      routerDatas.push(routerData)

      // recursive
      return this.getRouterData(child, url, routerDatas)
    }
    return routerDatas
  }

  logout() {
    const dialogMessage = `do you want to logout now?`
    const dialogConfig = { title: 'Logout?', okButton: 'Yes, Logout' }
    const dialogRef = this.customDialogService.confirm(dialogMessage, dialogConfig)
    dialogRef.subscribe((result) => {
      result && this.authService.logout()
    })
  }

  onClickExpanded() {

  }

}
