export interface MenuItem {
    name?: string;
    url?: string;
    icon?: string;
    children?: MenuItem[];
    isShowChildren?: boolean;
    isExpanded?: boolean;
}

export interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}

export interface RouterData {
    title: string;
    url: string;
}
