import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'store-front',
    pathMatch: 'full',
  },
  {
    path: 'store-front',
    loadChildren: () =>
      import('./modules/store-front/store-front.module').then((m) => m.StoreFrontModule),
    data: { title: 'Store Front' },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
