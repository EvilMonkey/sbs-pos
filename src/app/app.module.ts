import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { LayoutModule } from '@angular/cdk/layout'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatButtonModule } from '@angular/material/button'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatIconModule } from '@angular/material/icon'
import { MatListModule } from '@angular/material/list'
import { LayoutComponent } from './layout/layout.component'
import { NavbarComponent } from './layout/components/navbar/navbar.component'
import { CustomDialogModule } from './shared/dialog/dialog.module'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire'


const config = {
  apiKey: 'AIzaSyAG74GesKt21q8KlGb4J4WFCoGSAmZN5VY',
  authDomain: 'shiba-book-shop-eac29.firebaseapp.com',
  projectId: 'shiba-book-shop-eac29',
  storageBucket: 'shiba-book-shop-eac29.appspot.com',
  messagingSenderId: '440934580506',
  appId: '1:440934580506:web:ff65fdd8ed85e851acaca3'
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    NavbarComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    CustomDialogModule,
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
