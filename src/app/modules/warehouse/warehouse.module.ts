import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WarehouseComponent } from './warehouse.component';



@NgModule({
  declarations: [WarehouseComponent],
  imports: [
    CommonModule
  ]
})
export class WarehouseModule { }
