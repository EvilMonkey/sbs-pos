export interface Book {
    cover: string
    price: number
    title: string
    id: string
    discount?: number
}