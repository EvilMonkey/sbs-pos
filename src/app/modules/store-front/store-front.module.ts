import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './pages/main/main.component';
import { DisplayComponent } from './pages/display/display.component';
import { StoreFrontRoutingModule } from './store-front-routing.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductItemComponent } from './pages/main/components/product-item/product-item.component';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { ResultFormComponent } from './pages/main/components/result-form/result-form.component';
import { AmountInputComponent } from './pages/main/components/result-form/components/amount-input/amount-input.component';



@NgModule({
  declarations: [
    MainComponent,
    DisplayComponent,
    ProductItemComponent,
    ResultFormComponent,
    AmountInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    StoreFrontRoutingModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule
  ]
})
export class StoreFrontModule { }
