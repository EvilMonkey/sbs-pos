import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class StoreFrontService {

  protected bookApi = `https://api.jsonbin.io/b/5f3154b06f8e4e3faf2f99de`;

  // orderList: FirebaseListObservable<any[]> = null; //  list of objects
  // order: FirebaseObjectObservable<any> = null; //   single object

  constructor(private http: HttpClient) { }

  getProduct(): Observable<any> {
    return this.http.get(this.bookApi)
  }
}
