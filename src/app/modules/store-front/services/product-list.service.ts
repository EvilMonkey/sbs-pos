import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { ProductListModel } from '../models/product-list.model'

export interface Totals {
    subTotal: number;
    discount: number;
    grandTotal: number;
}

@Injectable({
    providedIn: 'root'
})
export class ProductListService {

    items: ProductListModel[] = []
    cartChanged = new Subject<ProductListModel[]>()

    constructor() { }

    getProductListItems() {
        return this.items.slice()
    }

    deleteProductListItem(index) {
        this.items.splice(index, 1)
        this.cartChanged.next(this.items.slice())
    }

    addProductListItems(item: ProductListModel) {
        this.items.push(item)
        this.cartChanged.next(this.items.slice())
    }
}
