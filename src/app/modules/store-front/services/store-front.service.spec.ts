import { TestBed } from '@angular/core/testing';

import { StoreFrontService } from './store-front.service';

describe('StoreFrontService', () => {
  let service: StoreFrontService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoreFrontService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
