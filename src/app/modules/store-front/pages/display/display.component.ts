import { Component, OnInit } from '@angular/core'
import { Book } from '../../interfaces/book.interface'
import * as _ from 'lodash'
import { Router } from '@angular/router'

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {

  public currentTotalResult = JSON.parse(localStorage.getItem('currentTotalResult'))
  public currentProductList = JSON.parse(localStorage.getItem('currentProductList'))

  public productListGroup: {
    key: string,
    value: Book[]
  }[]

  constructor(private router: Router) {
    if (!this.currentTotalResult || !this.currentProductList) { this.router.navigate([`store-front/main`]) }
  }

  ngOnInit(): void {
    const groupByTitle = _.groupBy(this.currentProductList, res => res.title)
    const groupKey = Object.keys(groupByTitle)
    this.productListGroup = groupKey.map(title => ({ key: title, value: groupByTitle[title] }))
  }

  onBack() {
    localStorage.clear()
    this.router.navigate([`store-front/main`])
  }
}
