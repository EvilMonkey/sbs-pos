import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Book } from 'src/app/modules/store-front/interfaces/book.interface';
import { CustomValidators } from 'src/app/shared/custom-validator';
import { CustomDialogService } from 'src/app/shared/dialog/dialog.service';
import { ProductUnitList } from '../../main.component';

@Component({
  selector: 'app-result-form',
  templateUrl: './result-form.component.html',
  styleUrls: ['./result-form.component.scss']
})
export class ResultFormComponent implements OnInit {

  get subtotal() {
    return this.totalResultForm.get('subtotal') as FormGroup;
  }

  get discount() {
    return this.totalResultForm.get('discount') as FormGroup;
  }

  get net() {
    return this.totalResultForm.get('net') as FormGroup;
  }

  get cuscredit() {
    return this.totalResultForm.get('cuscredit') as FormGroup;
  }

  get change() {
    return this.totalResultForm.get('change') as FormGroup;
  }

  @Output() addItem = new EventEmitter()
  @Output() removeItem = new EventEmitter()
  @Output() deleteItem = new EventEmitter()
  @Output() confirmOrder = new EventEmitter()

  @Input() totalResultForm: FormGroup

  _productListGroup: ProductUnitList[] = []

  @Output() productListGroupChange = new EventEmitter()

  @Input()
  get productListGroup() {
    return this._productListGroup
  }
  set productListGroup(value: ProductUnitList[]) {
    this.productListGroupChange.emit(value)
    this._productListGroup = value
  }

  @Input() formArray: FormArray


  public inputControl = new FormControl('', [Validators.required, CustomValidators.customerCreditCompair])

  constructor(private customDialogService: CustomDialogService,) { }

  ngOnInit(): void {
    console.log(this.productListGroup)
  }

  onAddItem(data: Book) {
    // console.log('onAddItem', data)
    this.addItem.emit(data)
  }

  onDeleteItem(data: Book) {
    this.deleteItem.emit(data.id)
  }

  onRemoveItem(data: Book) {
    this.removeItem.emit(data.id)
  }

  onConfirmItem() {
    if (this.totalResultForm.valid && (this.subtotal.value > 1)) {
      this.confirmOrder.emit()
    } else {
      this.customDialogService.alert(`Please select products before making a transaction.`)
    }
  }

}
