import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-amount-input',
  templateUrl: './amount-input.component.html',
  styleUrls: ['./amount-input.component.scss']
})
export class AmountInputComponent implements OnInit {

  @Output() clickAdd = new EventEmitter()
  @Output() clickRemove = new EventEmitter()
  @Output() clickDelete = new EventEmitter()

  // @Input() value: number

  _value: number

  @Output() valueChange = new EventEmitter()

  @Input()
  get value() {
    return this._value
  }
  set value(value: number) {
    this.valueChange.emit(value)
    this._value = value
  }

  constructor() { }

  ngOnInit(): void {
  }

}
