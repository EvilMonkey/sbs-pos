import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Book } from 'src/app/modules/store-front/interfaces/book.interface';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  @Input() item: Book | undefined
  @Input() isPromotion: boolean
  @Output() clickAddItem = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
    // console.log('ProductItemComponent', this.item)
  }

  onClickCard(itemValue: Book) {
    this.clickAddItem.emit(itemValue)
  }

}
