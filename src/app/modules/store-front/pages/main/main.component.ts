import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomDialogService } from 'src/app/shared/dialog/dialog.service';
import { Book } from '../../interfaces/book.interface';
import { StoreFrontService } from '../../services/store-front.service';
import * as _ from 'lodash'
import { CustomValidators } from 'src/app/shared/custom-validator';
import { Router } from '@angular/router';

export interface ProductUnitList {
  key: string,
  value: Book[]
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {

  get subtotal() {
    return this.totalResultForm.get('subtotal') as FormControl;
  }

  get discount() {
    return this.totalResultForm.get('discount') as FormControl;
  }

  get net() {
    return this.totalResultForm.get('net') as FormControl;
  }

  get cuscredit() {
    return this.totalResultForm.get('cuscredit') as FormControl;
  }

  get change() {
    return this.totalResultForm.get('change') as FormControl;
  }

  protected discountItem: string[] = [
    '9781408855652',
    '9781408855669',
    '9781408855676',
    '9781408855683',
    '9781408855690',
    '9781408855706',
    '9781408855713'
  ]

  protected discountList: { uniqAmount: number, discount: number }[] = [
    { uniqAmount: 2, discount: 10 },
    { uniqAmount: 3, discount: 11 },
    { uniqAmount: 4, discount: 12 },
    { uniqAmount: 5, discount: 13 },
    { uniqAmount: 6, discount: 14 },
    { uniqAmount: 7, discount: 15 },
  ]

  public bookList: Book[] = []
  public productList: Book[] = []
  public productListGroup: {
    key: string,
    value: Book[]
  }[]

  public totalResultForm: FormGroup

  public formArray: FormArray
  public productUnitList: ProductUnitList[]
  // public fornGroup: FormGroup

  constructor(
    private storeFrontService: StoreFrontService,
    private customDialogService: CustomDialogService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.totalResultForm = this.fb.group({
      subtotal: [0, Validators.required],
      discount: [0, Validators.required],
      net: [0, Validators.required],
      cuscredit: ['', Validators.required],
      change: 0,
    }, { validators: CustomValidators.customerCreditCompair })
    this.formArray = this.fb.array([])
  }

  ngOnInit(): void {

    this.cuscredit.valueChanges.subscribe(res => {
      const netPrice = this.net.value
      const changeValue = Number(res) - Number(netPrice)
      this.change.patchValue(changeValue)
    })

    this.storeFrontService.getProduct().subscribe((res: { books: Book[] }) => {
      const bookList = res.books ? res.books : undefined
      bookList ? this.bookList = bookList : this.customDialogService.alert('ไม่สามารถเรียกข้อมูลสินค้าได้ กรุณาตรวจสอบ')
      // console.log('getProduct', bookList)
    }, (err: HttpErrorResponse) => {
      this.customDialogService.alert('มีข้อผิดพลาดในการเชื่อมต่อ กรุณาตรวจสอบ')
      console.error(err)
    })
  }

  onDeleteItem(valueId: string) {
    _.remove(this.productList, data => data.id === valueId);
    this.onUpdateGroupItem()
  }

  onRemoveItem(valueId: string) {
    this.productList.splice(this.productList.findIndex(product => product.id === valueId), 1)
    this.onUpdateGroupItem()
  }

  onUpdateGroupItem() {
    const groupByTitle = _.groupBy(this.productList, res => res.title)
    const groupKey = Object.keys(groupByTitle)
    this.productListGroup = groupKey.map(title => ({ key: title, value: groupByTitle[title] }))
    this.calculateTotal()
  }

  onAddItem(itemValue: Book): void {
    this.productList.push(itemValue)
    this.onUpdateGroupItem()
    this.cuscredit.reset()
  }

  onConfirmOrder(): void {
    localStorage.setItem('currentProductList', JSON.stringify(this.productList))
    localStorage.setItem('currentTotalResult', JSON.stringify(this.totalResultForm.value))
    this.router.navigate([`store-front/display`]);
  }

  calculateTotal() {
    const groupById = _.groupBy(this.productList, res => res.id)
    // const groupKey = Object.keys(groupById)
    // const listgroup = groupKey.map(title => ({ key: title, value: groupKey[title] }))
    let subtotalVal: number = 0
    this.productList.forEach(product => {
      const { price } = product
      subtotalVal = Number(subtotalVal) + Number(price)
    })
    this.subtotal.patchValue(subtotalVal)

    const getDisount = (groupValue: any) => {
      const cleanNotPromotion = _.pick(groupValue, this.discountItem)
      const sortedArrayValue = _.sortBy(cleanNotPromotion, [(value => value.length)])
      const rotateArray = sortedArrayValue.map((_, colIndex) => sortedArrayValue.map(row => row[colIndex]))
      const productPromotionSet = rotateArray.map(list => _.remove(list, undefined))
      // console.log('productPromotionSet', productPromotionSet)
      let totaldiscount = 0

      productPromotionSet.forEach(setList => {
        const setListLength = setList.length
        if (setListLength > 1) {
          const findDiscount = this.discountList.find(discount => setListLength === discount.uniqAmount)
          setList.forEach((bookValue: Book) => {
            const getdiscountByBook = ((bookValue.price * findDiscount.discount) / 100)
            totaldiscount = (totaldiscount + getdiscountByBook)
          });
        }
      })

      return totaldiscount
    }
    // console.log('getDisount', getDisount(groupById))

    const getNetprice = (totalPrice: number, discount: number) => {
      return Number(totalPrice) - Number(discount)
    }
    this.discount.patchValue(getDisount(groupById))
    this.net.patchValue(getNetprice(subtotalVal, this.discount.value))



    // _.bindKey()
    // const resObject = groupKey.find(key => {
    //   this.discountItem.map(res => )
    // });


    // const netPrice = 

  }
}
