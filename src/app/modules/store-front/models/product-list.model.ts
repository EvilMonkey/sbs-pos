export class ProductListModel {
    name: string
    price: number
    quantity: number
}
