import { TestBed } from '@angular/core/testing';

import { BillManageService } from './bill-manage.service';

describe('BillManageService', () => {
  let service: BillManageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BillManageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
