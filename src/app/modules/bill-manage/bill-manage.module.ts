import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillManageComponent } from './bill-manage.component';



@NgModule({
  declarations: [BillManageComponent],
  imports: [
    CommonModule
  ]
})
export class BillManageModule { }
