import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialog } from './confirm';
import { AlertDialog } from './alert';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { CustomDialogService } from './dialog.service';



@NgModule({
  declarations: [
    ConfirmDialog,
    AlertDialog
  ],
  imports: [
    CommonModule, MatDialogModule, MatButtonModule
  ],
  providers: [CustomDialogService]
})
export class CustomDialogModule { }
