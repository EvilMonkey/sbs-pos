import { Injectable } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { Observable } from 'rxjs'
import { AlertDialog } from './alert'
import { ConfirmDialog } from './confirm'


export declare type ThemePalette = 'primary' | 'accent' | 'warn' | undefined

export interface DialogConfig {
  title?: string
  okButton?: string
  cancelButton?: string
  titleLevel?: ThemePalette
  buttonLevel?: ThemePalette
}

@Injectable({
  providedIn: 'root'
})
export class CustomDialogService {

  constructor(private dialog: MatDialog) { }

  confirm(message: string, config?: DialogConfig): Observable<any> {
    const dialogRef = this.dialog.open(ConfirmDialog, { disableClose: false, data: { message, config }, width: '400px' })
    return dialogRef.afterClosed()
  }

  alert(message: string, config?: DialogConfig): Observable<any> {
    const dialogRef = this.dialog.open(AlertDialog, { disableClose: false, data: { message, config }, width: '400px' })
    return dialogRef.afterClosed()
  }

}
