import { Component, Input, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { DialogConfig } from './dialog.service'

export interface DialogData {
    message: string
    config?: DialogConfig
}

// text,email,tel,textarea,password,
@Component({
    selector: 'alert-dialog',
    template: `
            <h1 mat-dialog-title>{{data.config?.title? data.config.title: 'Alert!'}}</h1>
            <div mat-dialog-content class="mat-typography"><p>{{data.message}}</p></div>
            <div mat-dialog-actions align="end" class="mb-0">
                <button mat-raised-button mat-dialog-close>{{data.config?.okButton? data.config.okButton: 'Ok'}}</button>
            </div>
    `
})
// tslint:disable-next-line: component-class-suffix
export class AlertDialog {
    constructor(public dialogRef: MatDialogRef<AlertDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {
        // console.log('dialogRef', dialogRef)
        // console.log('data', data)
    }
}
