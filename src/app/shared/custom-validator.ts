import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

export class CustomValidators extends Validators {

    static customerCreditCompair(control: FormControl) {
        const netPrice = control.get('net') as FormControl
        const cuscredit = control.get('cuscredit') as FormControl

        if (cuscredit && netPrice) {
            // console.log('netPrice', netPrice.value)
            // console.log('cuscredit', cuscredit.value)
            return cuscredit.value < netPrice.value ? { 'wrongCredit': true } : null
        } else {
            return null
        }

    }
}